ARG VERSION=18.04
FROM ubuntu:$VERSION
RUN apt-get update \
&& apt-get install -y \
nginx \
&& mkdir /var/run/socket \
&& rm -f /etc/nginx/sites-available/default
COPY  default /etc/nginx/sites-available/default
EXPOSE 80/tcp
CMD ["nginx", "-g", "daemon off;"]
